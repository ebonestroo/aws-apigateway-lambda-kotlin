package org.example

import kotlinx.serialization.Serializable

@Serializable
data class HelloWorldResponse(val response: String) {
}