package org.example


import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.LambdaLogger
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.lambda.runtime.events.*
import kotlinx.serialization.*
import kotlinx.serialization.json.*
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import software.amazon.awssdk.services.dynamodb.model.*
import java.util.UUID


private val tableKey = System.getenv("TABLE_NAME")

class Handler : RequestHandler<APIGatewayV2HTTPEvent, APIGatewayV2HTTPResponse> {
    override fun handleRequest(event: APIGatewayV2HTTPEvent?, context: Context?): APIGatewayV2HTTPResponse {
        val logger: LambdaLogger = context?.getLogger()!!;
        logger.log("EVENT TYPE: " + event?.javaClass.toString());
        val response = APIGatewayV2HTTPResponse();
        response.setIsBase64Encoded(false);
        response.setStatusCode(200);
        val headers: HashMap<String, String> = HashMap();
        headers.put("Content-Type", "application/json");
        response.setHeaders(headers)
        val body = event?.let { Json.decodeFromString<HelloWorldRequest>(it.body) } ?: HelloWorldRequest("empty request")


        val responseBody = HelloWorldResponse("Hello ${body.request}")
        putItemInTable(body.request,tableKey)
        response.setBody(Json.encodeToString(responseBody))
        return response;
    }



    fun putItemInTable(
        name: String,
        tableName: String,
    ) {
        println("adding $name")
        val ddb = DynamoDbClient.builder()
            .region(Region.EU_CENTRAL_1)
            .build()

        println("client build")
        val itemValues = HashMap<String, AttributeValue>()
        itemValues["id"] = AttributeValue.builder().s(UUID.randomUUID().toString()).build()
        itemValues["name"] = AttributeValue.builder().s(name).build()

        val request = PutItemRequest.builder()
            .tableName(tableName)
            .item(itemValues)
            .build()
        println("do put item")
        try {
            val response: PutItemResponse = ddb.putItem(request)
            println(
                tableName + " was successfully updated. The request id is " + response.responseMetadata().requestId()
            )
        } catch (e: ResourceNotFoundException) {
            System.err.format("Error: The Amazon DynamoDB table \"%s\" can't be found.\n", tableName)
            System.err.println("Be sure that it exists and that you've typed its name correctly!")
            System.exit(1)
        } catch (e: DynamoDbException) {
            System.err.println(e.message)
            System.exit(1)
        }
    }
}