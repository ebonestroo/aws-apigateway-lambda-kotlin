package org.example

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.LambdaLogger
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2HTTPEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2HTTPResponse
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import software.amazon.awssdk.services.dynamodb.model.*
import java.util.*


private val tableKey = System.getenv("TABLE_NAME")

class GetHandler : RequestHandler<APIGatewayV2HTTPEvent, APIGatewayV2HTTPResponse> {
    override fun handleRequest(event: APIGatewayV2HTTPEvent?, context: Context?): APIGatewayV2HTTPResponse {
        val logger: LambdaLogger = context?.getLogger()!!;
        logger.log("EVENT TYPE: " + event?.javaClass.toString());
        val response = APIGatewayV2HTTPResponse();
        response.setIsBase64Encoded(false);
        response.setStatusCode(200);
        val headers: HashMap<String, String> = HashMap();
        headers.put("Content-Type", "application/json");
        response.setHeaders(headers)

        val ddb = DynamoDbClient.builder()
            .region(Region.EU_CENTRAL_1)
            .build()

        val result = scanItems(ddb, tableKey)


        response.setBody(Json.encodeToString(result))
        return response;
    }

    fun scanItems(ddb: DynamoDbClient, tableName: String?): List<HelloWorldResponse> {
        try {
            val scanRequest = ScanRequest.builder()
                .tableName(tableName)
                .build()
            val response = ddb.scan(scanRequest)
            return response.items().map { HelloWorldResponse(response = it.get("name")?.s() ?: "Geen naam") }
        } catch (e: DynamoDbException) {
            e.printStackTrace()
        }
        return emptyList()
    }

}