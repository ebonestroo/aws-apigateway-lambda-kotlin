package org.example

import kotlinx.serialization.Serializable

@Serializable
data class HelloWorldRequest(val request: String) {
}